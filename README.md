# README

## Prerequisites
- Laravel 5
- Node.js
- Composer

## How to run locally
1. Clone the repository by running `git clone https://imperets@bitbucket.org/imperets/jell-test-app.git`
2. Navigate into the project directory `cd jell-test-app`
3. Install necessary dependencies by running `npm install`
4. Create a new database locally
5. Rename the `.env.example` file to `.env` and fill in the correct database details
6. Run `composer install`
7. Run `php artisan migrate:refresh --seed` to migrate and seed the database
8. Run `php artisan key:generate` to generate an app encryption key
9. Run `php artisan serve` to serve the app locally
10. Login to the app with `admin@test.com` as the username and `password` as the password
11. You may also register a new user by clicking "Register" in the home view
