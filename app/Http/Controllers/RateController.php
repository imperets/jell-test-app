<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;

class RateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return Rate::all();
    }
}
