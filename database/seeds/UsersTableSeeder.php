<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $password = Hash::make('password');
        $amount = 10;

        function randomString($length) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        User::create([
            'email' => 'admin@test.com',
            'password' => $password,
            'api_token' => 'x1tzpq096qy4GJP1joooRastBiuokSimVX0u5vaMbmEu6mA60StRoF6Lkiz8'
        ]);

        for ($i = 0; $i < $amount; $i++) {
            User::create([
                'email' => $faker->email,
                'password' => $password,
                'api_token' => randomString(60)
            ]);
        }
    }
}
