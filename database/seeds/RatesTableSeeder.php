<?php

use Illuminate\Database\Seeder;
use App\Rate;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rate::create([
            'id' => 1,
            'label' => 'Development',
            'rate' => 160,
        ]);

        Rate::create([
            'id' => 2,
            'label' => 'Design',
            'rate' => 120,
        ]);
        Rate::create([
            'id' => 3,
            'label' => 'Project Management',
            'rate' => 120,
        ]);

        Rate::create([
            'id' => 4,
            'label' => 'Travel',
            'rate' => 100,
        ]);
    }
}
