import axios from 'axios'

class Api {

    static getAll(context){
        return axios({
            url: `api/${context}`,
            method: 'GET',
            dataType: 'json',
            headers: {
                'Authorization': `Bearer ${window.apiToken}`
            },
        })
            .then(response => response)
            .catch(error => console.error(error))
    }

}

export default Api;