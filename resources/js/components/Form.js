import React from 'react'

const Form = ({state, data, handleRateChange, handleTimeChange, handleUnitChange, handleCostChange, handleRound, setCost}) => (
    <div className="container-fluid">
        <div className="row m-auto form-wrapper">
            <h3 className="m-auto pb-3">Cost/Time Calculator</h3>
            <form>
                <div className="row mb-2">
                    <div className="col-sm-2">
                        <label className="" htmlFor="worktypeSelect">Work</label>
                    </div>
                    <div className="col-sm-10">
                        <select className="custom-select"
                                id="worktypeSelect"
                                value={state.rate}
                                onChange={handleRateChange}>
                            {data ? data.map(row => (
                                <option key={row.id}
                                        value={row.id}>{`${row.label} ($${row.rate}/hour)`}</option>
                            )) : ''}
                        </select>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col-sm-2">
                        <label className="" htmlFor="timeInput">Time</label>
                    </div>
                    <div className="col-sm-10 input-group">
                        <input type="number"
                               name="time"
                               className="form-control"
                               id="timeInput"
                               min="0"
                               value={state.time}
                               onChange={handleTimeChange}/>
                        <select className="custom-select input-group-prepend"
                                id="timeSelect"
                                value={state.unit}
                                onChange={handleUnitChange}>
                            <option value="60">Minutes</option>
                            <option value="1">Hours</option>
                        </select>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col-sm-2">
                        <label className="" htmlFor="costInput">Cost</label>
                    </div>
                    <div className="col-sm-10">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">$</span>
                            </div>
                            <input type="number"
                                   name="cost"
                                   className="form-control"
                                   id="costInput"
                                   min="0"
                                   value={state.cost}
                                   onBlur={setCost}
                                   onChange={handleCostChange} />
                        </div>
                    </div>
                </div>
                <div className="btn-group btn-block mt-3" role="group">
                    <button type="button"
                            className="btn btn-outline-dark"
                            onClick={() => handleRound('down')}>Round down time</button>
                    <button type="button"
                            className="btn btn-outline-dark"
                            onClick={() => handleRound('up')}>Round up time</button>
                </div>
            </form>
        </div>
    </div>
)

export default Form;