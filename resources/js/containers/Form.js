import React from 'react'
import Form from '../components/Form'

class FormContainer extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            rateId: 1,
            time: 60,
            unit: 60, //set 60 for minutes, 1 for hours
            cost: '160.00'
        };
        this.handleRateChange = this.handleRateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleUnitChange = this.handleUnitChange.bind(this);
        this.handleCostChange = this.handleCostChange.bind(this);
        this.setCost = this.setCost.bind(this);
        this.handleRound = this.handleRound.bind(this);
    }

    currentRate(rateId){
        return this.props.rate(rateId, this.props.rates);
    }

    cost(time, rateId){
        return (time / this.state.unit * this.currentRate(rateId)).toFixed(2)
    }

    setCost(){
        this.setState({cost: parseFloat(this.state.cost).toFixed(2)})
    }

    formattedTime(num){
        return Math.round(num * 100) / 100;
    }

    roundedTime(direction, unit){
        const roundedMinutes = direction === 'up'
            ? Math.ceil(this.state.time / 15) * 15
            : Math.floor(this.state.time / 15) * 15;
        const roundedHours = direction === 'up'
            ? Math.ceil(this.state.time / .25) * .25
            : Math.floor(this.state.time / .25) * .25;
        return unit == 60 ? roundedMinutes : roundedHours
    }

    handleRateChange(event){
        const rateId = event.target.value;
        this.setState({
            rateId: rateId,
            cost: this.cost(this.state.time, rateId)
        })
    }

    handleTimeChange(event){
        const time = event.target.value;
        this.setState({
            time: time,
            cost: this.cost(time, this.state.rateId)
        });
    }

    handleUnitChange(event){
        const unit = event.target.value;
        const time = this.state.time;
        this.setState({
            unit: unit,
            time: unit == 60 ? this.formattedTime(time * 60) : this.formattedTime(time / 60)
        })
    }

    handleCostChange(event){
        const cost = event.target.value;
        this.setState({
            cost: cost,
            time: this.formattedTime(cost / this.currentRate(this.state.rateId) * this.state.unit)
        })
    }

    handleRound(direction){
        const roundedTime = this.roundedTime(direction, this.state.unit);
        this.setState({
            time: roundedTime,
            cost: this.cost(roundedTime, this.state.rateId)
        })
    }

    render(){
        return(
            <Form data = {this.props.rates}
                  state = {this.state}
                  handleRateChange={this.handleRateChange}
                  handleTimeChange={this.handleTimeChange}
                  handleUnitChange={this.handleUnitChange}
                  handleCostChange={this.handleCostChange}
                  handleRound={this.handleRound}
                  setCost={this.setCost} />
        )
    }

}

export default FormContainer;