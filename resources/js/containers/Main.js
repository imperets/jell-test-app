import React from 'react'
import ReactDOM from 'react-dom'
import FormContainer from './Form'
import Api from '../Api'

class Main extends React.Component {

    constructor(){
        super();
        this.state = {};
    }

    getRates(){
        Api.getAll('rates')
            .then(response => {
                this.setState({
                    rates: response.data,
                });
            })
    }

    rate(id, rates){
        return rates.find(row => parseInt(row.id) === parseInt(id)).rate
    }

    componentDidMount(){
        this.getRates();
    }

    render(){
        return(
            <div className="main-wrapper">
                <FormContainer rates={this.state.rates}
                               rate={this.rate} />
            </div>
        )
    }

}

ReactDOM.render(<Main />, document.getElementById('app'));